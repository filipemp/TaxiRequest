//
//  PinManagerTests.m
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 8/25/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "PinManager.h"
@import GoogleMaps;

@interface PinManagerTests : XCTestCase

@property(strong, nonatomic) GMSMapView *mapView;
@end

@implementation PinManagerTests

- (void)setUp {
  [super setUp];
  [GMSServices provideAPIKey:@"AIzaSyBe4BvBtCsHQ8oAorwPc7LeoZhUmZkSYYI"];
  GMSCameraPosition *camera =
      [GMSCameraPosition cameraWithLatitude:1.285 longitude:103.848 zoom:12];
  self.mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
}

- (void)tearDown {
  [self.mapView clear];
  self.mapView = nil;
  [[[PinManager sharedManager] markers] removeAllObjects];
  [super tearDown];
}

- (void)testAddingTaxiAsPins {
  CLLocation *taxi1 = [[CLLocation alloc] initWithLatitude:-3.6799280139059922
                                                 longitude:-46.592817981043105];
  CLLocation *taxi2 = [[CLLocation alloc] initWithLatitude:-23.6799280139059922
                                                 longitude:-46.592817981043105];
  CLLocation *taxi3 = [[CLLocation alloc] initWithLatitude:-23.6799280139059922
                                                 longitude:-6.592817981043105];
  NSArray *taxis = @[ taxi1, taxi2, taxi3 ];
  [[PinManager sharedManager] addTaxiPins:taxis atMapView:self.mapView];

  XCTAssert([[PinManager sharedManager] markers].count == 3);
  for (GMSMarker *marker in [[PinManager sharedManager] markers]) {
    XCTAssert(marker.map == self.mapView);
  }
}

- (void)testAddingNoTaxisAsPins {
  NSArray *taxis = @[];
  [[PinManager sharedManager] addTaxiPins:taxis atMapView:self.mapView];
  XCTAssert([[PinManager sharedManager] markers].count == 0);
}

- (void)testAddingTaxiAsPin {
  GMSMarker *taxi = [[PinManager sharedManager]
      createTaxiMarkerWithCoordinate:CLLocationCoordinate2DMake(
                                         -23.6799280139059922,
                                         -6.592817981043105)
                           atMapView:self.mapView];
  XCTAssert([[PinManager sharedManager] markers].count == 1);
  XCTAssert(taxi.map == self.mapView);
}

- (void)testClearingUpMapView {
  CLLocation *taxi1 = [[CLLocation alloc] initWithLatitude:-3.6799280139059922
                                                 longitude:-46.592817981043105];
  CLLocation *taxi2 = [[CLLocation alloc] initWithLatitude:-23.6799280139059922
                                                 longitude:-46.592817981043105];
  CLLocation *taxi3 = [[CLLocation alloc] initWithLatitude:-23.6799280139059922
                                                 longitude:-6.592817981043105];
  NSArray *taxis = @[ taxi1, taxi2, taxi3 ];
  [[PinManager sharedManager] addTaxiPins:taxis atMapView:self.mapView];

  XCTAssert([[PinManager sharedManager] markers].count == 3);
  [[PinManager sharedManager] clearTaxis];
  XCTAssert([[PinManager sharedManager] markers].count == 0);
}

- (void)testAddingUserPinAndUpdatingIt {
  [[PinManager sharedManager]
      addOrUpdateUserMarker:CLLocationCoordinate2DMake(-23.6799280139059922,
                                                       -46.592817981043105)
                  atMapView:self.mapView];

  XCTAssertNotNil([[PinManager sharedManager] userMarker]);

  CLLocation *userLocation =
      [[CLLocation alloc] initWithLatitude:-23.6799280139059922
                                 longitude:-46.592817981043105];
  CLLocation *markerLocation = [[CLLocation alloc]
      initWithLatitude:[[PinManager sharedManager] userMarker].position.latitude
             longitude:[[PinManager sharedManager] userMarker]
                           .position.longitude];
  XCTAssert([userLocation distanceFromLocation:markerLocation] < 1.0);

  [[PinManager sharedManager]
      addOrUpdateUserMarker:CLLocationCoordinate2DMake(-23.6799280139059922,
                                                       -6.592817981043105)
                  atMapView:self.mapView];

  userLocation = [[CLLocation alloc] initWithLatitude:-23.6799280139059922
                                            longitude:-6.592817981043105];
  markerLocation = [[CLLocation alloc]
      initWithLatitude:[[PinManager sharedManager] userMarker].position.latitude
             longitude:[[PinManager sharedManager] userMarker]
                           .position.longitude];
  XCTAssert([userLocation distanceFromLocation:markerLocation] < 1.0);
}

@end
