//
//  TaxiLoaderTestes.m
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 8/25/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <CoreLocation/CoreLocation.h>
#import "TaxiLoader.h"

@import GoogleMaps;

#define SP CLLocationCoordinate2DMake(-23.6824124, -46.5952992)
#define CLOSEST_TAXI_POSITION                                                  \
  CLLocationCoordinate2DMake(-23.676973146257232, -46.58404766335911)

@interface TaxiRequestTests : XCTestCase

@property(strong, nonatomic) CLLocation *location;

@end

@implementation TaxiRequestTests

- (void)setUp {
  [super setUp];

  self.location =
      [[CLLocation alloc] initWithLatitude:SP.latitude longitude:SP.longitude];
}

- (void)tearDown {
  self.location = nil;
  [super tearDown];
}

- (void)testTaxiLoaderForCoordinate {
  XCTestExpectation *requestFinishedExpectation =
      [self expectationWithDescription:@"request finished"];

  [TaxiLoader loadTaxisWithCoordinate:self.location.coordinate
      success:^(NSArray *taxis) {
        if (taxis && taxis.count > 0) {
          XCTAssert(true);
        }
        [requestFinishedExpectation fulfill];
      }
      failure:^(NSString *message) {
        if (message) {
          XCTAssert(true);
        }
        [requestFinishedExpectation fulfill];
      }];

  [self waitForExpectationsWithTimeout:1.0 handler:nil];
}

- (void)testClosesTaxi {
  GMSMarker *taxiOne = [[GMSMarker alloc] init];
  taxiOne.position =
      CLLocationCoordinate2DMake(-3.6799280139059922, -46.592817981043105);

  GMSMarker *taxiTwo = [[GMSMarker alloc] init];
  taxiTwo.position =
      CLLocationCoordinate2DMake(-3.6799280139059922, -6.592817981043105);

  GMSMarker *taxiThree = [[GMSMarker alloc] init];
  taxiThree.position =
      CLLocationCoordinate2DMake(-23.676973146257232, -46.58404766335911);

  NSArray *taxis = @[ taxiOne, taxiTwo, taxiThree ];
  CLLocationCoordinate2D closestTaxiCoordinate =
      [TaxiLoader findClosestTaxi:taxis toUser:SP];
  CLLocation *closestTaxiLocation =
      [[CLLocation alloc] initWithLatitude:closestTaxiCoordinate.latitude
                                 longitude:closestTaxiCoordinate.longitude];

  CLLocation *closestTaxiLocationForSure =
      [[CLLocation alloc] initWithLatitude:CLOSEST_TAXI_POSITION.latitude
                                 longitude:CLOSEST_TAXI_POSITION.longitude];

  XCTAssert(
      [closestTaxiLocation distanceFromLocation:closestTaxiLocationForSure] <
      1.0);
}

@end
