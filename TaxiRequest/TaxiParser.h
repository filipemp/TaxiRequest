//
//  TaxiParser.h
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 9/1/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@import CoreLocation.CLLocation;

@interface TaxiParser : NSObject

+ (void)parseData:(NSData *)data
          success:(void (^)(NSArray *taxis))success
          failure:(void (^)(NSString *message))failure;
@end
