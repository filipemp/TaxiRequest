//
//  PinManager.m
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 8/25/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import "PinManager.h"

@implementation PinManager

+ (PinManager *)sharedManager {
  static PinManager *sharedManager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedManager = [[self alloc] init];
  });
  return sharedManager;
}

- (instancetype)init {
  if (self = [super init]) {
    _markers = [NSMutableArray array];
  }
  return self;
}

- (void)addTaxiPins:(NSArray *)taxis atMapView:(GMSMapView *)mapView {
  for (CLLocation *location in taxis) {
    [self createTaxiMarkerWithCoordinate:location.coordinate atMapView:mapView];
  }
}

- (GMSMarker *)createTaxiMarkerWithCoordinate:(CLLocationCoordinate2D)coordinate
                                    atMapView:(GMSMapView *)mapView {
  GMSMarker *marker = [[GMSMarker alloc] init];
  marker.icon = [GMSMarker markerImageWithColor:[UIColor greenColor]];
  marker.position = coordinate;
  marker.title = @"Taxi";
  marker.snippet = @"Brasil";
  marker.map = mapView;

  [self.markers addObject:marker];

  return marker;
}

- (void)clearTaxis {
  for (GMSMarker *marker in self.markers) {
    marker.map = nil;
  }
  [self.markers removeAllObjects];
}

- (BOOL)hasTaxis {
  return _markers.count > 0;
}

- (void)addOrUpdateUserMarker:(CLLocationCoordinate2D)coordinates
                    atMapView:(GMSMapView *)mapView {
  if (!self.userMarker) {
    self.userMarker = [[GMSMarker alloc] init];
    self.userMarker.icon =
        [GMSMarker markerImageWithColor:[UIColor blackColor]];
    self.userMarker.draggable = YES;
    self.userMarker.title = @"User";
    self.userMarker.snippet = @"Brasil";
    self.userMarker.position = coordinates;
    self.userMarker.map = mapView;
  } else {
    [self updateUserMarker:coordinates];
  }
}

- (void)updateUserMarker:(CLLocationCoordinate2D)coordinates {
  self.userMarker.position = coordinates;
}

@end
