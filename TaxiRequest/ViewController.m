//
//  ViewController.m
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 8/21/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import "ViewController.h"

@import GoogleMaps;

#import "TaxiLoader.h"
#import "PinManager.h"
#import "MessageController.h"
#import "ReverseGeocoder.h"

@interface ViewController () <CLLocationManagerDelegate, GMSMapViewDelegate>

@property(weak, nonatomic) IBOutlet GMSMapView *mapView;
@property(strong, nonatomic) CLLocationManager *locationManager;
@property(weak, nonatomic) IBOutlet UILabel *addressLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
  [super viewDidLoad];

  GMSCameraPosition *camera =
      [GMSCameraPosition cameraWithLatitude:-23.68 longitude:-46.59 zoom:15];
  _mapView.camera = camera;
  _mapView.delegate = self;

  _locationManager = [CLLocationManager new];
  _locationManager.delegate = self;
  _locationManager.distanceFilter = kCLDistanceFilterNone;
  _locationManager.desiredAccuracy = kCLLocationAccuracyBest;

  [_locationManager requestWhenInUseAuthorization];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
  CLLocation *location = [locations lastObject];
  NSLog(@"User: lat%f - lon%f", location.coordinate.latitude,
        location.coordinate.longitude);

  [[PinManager sharedManager] addOrUpdateUserMarker:location.coordinate
                                          atMapView:_mapView];
  [_locationManager stopUpdatingLocation];

  [self updateScreen:location.coordinate];
}

- (void)locationManager:(CLLocationManager *)manager
    didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
  switch (status) {
  case kCLAuthorizationStatusNotDetermined: {
    NSLog(@"User still thinking..");
  } break;
  case kCLAuthorizationStatusDenied: {
    NSLog(@"User hates you");
  } break;
  case kCLAuthorizationStatusAuthorizedWhenInUse:
  case kCLAuthorizationStatusAuthorizedAlways: {
    [_locationManager startUpdatingLocation]; // Will update location
                                              // immediately
  } break;
  default:
    break;
  }
}

#pragma mark - GMSMapViewDelegate
- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker {
  [self updateScreen:marker.position];
}

#pragma mark - Private
- (void)updateScreen:(CLLocationCoordinate2D)coordinate {
  [[PinManager sharedManager] clearTaxis];
  [_mapView animateToLocation:coordinate];
  [ReverseGeocoder findAddressForCoordinate:coordinate withLabel:_addressLabel];
  [TaxiLoader loadTaxisWithCoordinate:coordinate
      success:^(NSArray *taxis) {
        dispatch_async(dispatch_get_main_queue(), ^{
          [[PinManager sharedManager] addTaxiPins:taxis atMapView:_mapView];
        });
      }
      failure:^(NSString *message) {
        [self showFailureAlertWithMessage:message];
      }];
}

- (void)showFailureAlertWithMessage:(NSString *)message {
  UIAlertController *alert =
      [MessageController buildFailureAlertWithMessage:message];
  [self presentViewController:alert animated:YES completion:nil];
}

- (void)showNoTaxisAvailableAlert {
  UIAlertController *alert = [MessageController buildNoTaxisAvailableAlert];

  [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Button Actions
- (IBAction)loadNearestTaxi:(UIButton *)sender {
  if ([[PinManager sharedManager] hasTaxis]) {
    CLLocationCoordinate2D userCoordinate =
        [[PinManager sharedManager] userMarker].position;
    NSArray *markers = [[PinManager sharedManager] markers];

    CLLocationCoordinate2D nearestTaxicoordinate =
        [TaxiLoader findClosestTaxi:markers toUser:userCoordinate];
    [[PinManager sharedManager] clearTaxis];
    [[PinManager sharedManager]
        createTaxiMarkerWithCoordinate:nearestTaxicoordinate
                             atMapView:_mapView];
  } else {
    [self showNoTaxisAvailableAlert];
  }
}

- (IBAction)goBackToInitialPosition:(UIButton *)sender {
  if ([CLLocationManager authorizationStatus] ==
      kCLAuthorizationStatusAuthorizedWhenInUse) {
    [_locationManager startUpdatingLocation];
  }
}

@end
