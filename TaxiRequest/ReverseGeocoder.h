//
//  ReverseGeocoder.h
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 9/1/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation.CLLocation;
@import UIKit.UILabel;

@interface ReverseGeocoder : NSObject

+ (void)findAddressForCoordinate:(CLLocationCoordinate2D)coordinate
                       withLabel:(UILabel *)label;

@end
