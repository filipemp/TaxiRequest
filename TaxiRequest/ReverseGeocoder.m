//
//  ReverseGeocoder.m
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 9/1/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import "ReverseGeocoder.h"
@import GoogleMaps.GMSGeocoder;

@implementation ReverseGeocoder

+ (void)findAddressForCoordinate:(CLLocationCoordinate2D)coordinate
                       withLabel:(UILabel *)label {
  [[GMSGeocoder geocoder]
      reverseGeocodeCoordinate:coordinate
             completionHandler:^(GMSReverseGeocodeResponse *response,
                                 NSError *error) {
               if (!error) {
                 GMSAddress *address = response.firstResult;
                 label.text = [address.lines firstObject];
               } else {
                 label.text = @"Sem endereço encontrado";
               }
             }];
}

@end
