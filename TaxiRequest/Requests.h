//
//  Requests.h
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 9/1/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation.CLLocation;

@interface Requests : NSObject

+ (void)getTaxisForCoordinate:(CLLocationCoordinate2D)coordinate
                    withBlock:
                        (void (^)(NSArray *taxis, NSString *message))block;

@end
