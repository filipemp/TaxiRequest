//
//  PinManager.h
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 8/25/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GoogleMaps;

@interface PinManager : NSObject

+ (PinManager *)sharedManager;

- (void)addTaxiPins:(NSArray *)taxis atMapView:(GMSMapView *)mapView;
- (GMSMarker *)createTaxiMarkerWithCoordinate:(CLLocationCoordinate2D)coordinate
                                    atMapView:(GMSMapView *)mapView;
- (BOOL)hasTaxis;
- (void)clearTaxis;
- (void)addOrUpdateUserMarker:(CLLocationCoordinate2D)coordinates
                    atMapView:(GMSMapView *)mapView;

@property(strong, nonatomic) NSMutableArray *markers;
@property(strong, nonatomic) GMSMarker *userMarker;

@end
