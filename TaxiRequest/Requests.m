//
//  Requests.m
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 9/1/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import "Requests.h"
#import "TaxiParser.h"

#define BASE_URL "http://quasinada-ryu.easytaxi.net.br/api/gettaxis?lat="

@implementation Requests

+ (void)getTaxisForCoordinate:(CLLocationCoordinate2D)coordinate
                    withBlock:
                        (void (^)(NSArray *taxis, NSString *message))block {

  NSString *formattedURL =
      [NSString stringWithFormat:@"%s%f&lng=%f", BASE_URL, coordinate.latitude,
                                 coordinate.longitude];
  NSURL *url = [NSURL URLWithString:formattedURL];

  NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession]
        dataTaskWithURL:url
      completionHandler:^(NSData *data, NSURLResponse *response,
                          NSError *error) {
        NSHTTPURLResponse *httpURLResponse = (NSHTTPURLResponse *)response;

        if ([httpURLResponse statusCode] >= 400) {
          block(nil, @"Error requesting the API data");
        } else if ([httpURLResponse statusCode] == 200) {
          [TaxiParser parseData:data
              success:^(NSArray *taxis) {
                block(taxis, nil);
              }
              failure:^(NSString *message) {
                block(nil, message);
              }];

        } else {
          block(nil, @"Connection Problem");
        }
      }];

  [downloadTask resume];
}

@end
