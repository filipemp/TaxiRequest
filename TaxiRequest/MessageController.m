//
//  MessageController.m
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 9/1/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

@import UIKit;

#import "MessageController.h"

@implementation MessageController

+ (UIAlertController *)buildFailureAlertWithMessage:(NSString *)message {
  UIAlertController *alert =
      [UIAlertController alertControllerWithTitle:@"Taxi Request"
                                          message:message
                                   preferredStyle:UIAlertControllerStyleAlert];

  UIAlertAction *defaultAction =
      [UIAlertAction actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction *action){
                             }];

  [alert addAction:defaultAction];
  return alert;
}

+ (UIAlertController *)buildNoTaxisAvailableAlert {
  UIAlertController *alert =
      [UIAlertController alertControllerWithTitle:@"Taxi Request"
                                          message:@"No taxis available"
                                   preferredStyle:UIAlertControllerStyleAlert];

  UIAlertAction *defaultAction =
      [UIAlertAction actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction *action){
                             }];

  [alert addAction:defaultAction];
  return alert;
}

@end
