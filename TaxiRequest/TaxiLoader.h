//
//  TaxiLoader.h
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 8/21/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@import CoreLocation;

@interface TaxiLoader : NSObject

+ (void)loadTaxisWithCoordinate:(CLLocationCoordinate2D)coordinate
                        success:(void (^)(NSArray *taxis))success
                        failure:(void (^)(NSString *message))failure;
+ (CLLocationCoordinate2D)findClosestTaxi:(NSArray *)taxis
                                   toUser:
                                       (CLLocationCoordinate2D)userCoordinate;

@end
