//
//  AppDelegate.h
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 8/21/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) UIWindow *window;

@end
