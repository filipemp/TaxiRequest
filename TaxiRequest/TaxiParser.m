//
//  TaxiParser.m
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 9/1/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import "TaxiParser.h"

@implementation TaxiParser

+ (CLLocationCoordinate2D)parseSingleTaxiDictionary:(NSDictionary *)taxiDict {
  double lat = [taxiDict[@"lat"] doubleValue];
  double lng = [taxiDict[@"lng"] doubleValue];
  CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat, lng);
  return coordinate;
}

+ (void)parseData:(NSData *)data
          success:(void (^)(NSArray *taxis))success
          failure:(void (^)(NSString *message))failure {
  NSError *jsonError;

  // Parse the JSON response
  NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                       options:kNilOptions
                                                         error:&jsonError];

  NSString *errorMessage;
  if (jsonError) {
    errorMessage = @"Error while requesting taxis";
  } else if (dict.count == 0) {
    errorMessage = @"No results found";
  } else if (success) {
    success([TaxiParser parseTaxisDictionary:dict]);
  }

  if (errorMessage && failure) {
    failure(errorMessage);
  }
}

+ (NSMutableArray *)parseTaxisDictionary:(NSDictionary *)dictionary {
  NSMutableArray *retVal = [NSMutableArray array];
  NSArray *taxis = (NSArray *)dictionary[@"taxis"];
  for (NSDictionary *taxi in taxis) {
    CLLocationCoordinate2D coordinate =
        [TaxiParser parseSingleTaxiDictionary:taxi];
    [retVal
        addObject:[[CLLocation alloc] initWithLatitude:coordinate.latitude
                                             longitude:coordinate.longitude]];
  }
  return retVal;
}

@end
