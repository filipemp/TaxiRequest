//
//  MessageController.h
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 9/1/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageController : NSObject

+ (UIAlertController *)buildFailureAlertWithMessage:(NSString *)message;
+ (UIAlertController *)buildNoTaxisAvailableAlert;

@end
