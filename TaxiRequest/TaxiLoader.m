//
//  TaxiLoader.m
//  TaxiRequest
//
//  Created by Filipe Marques Pereira on 8/21/15.
//  Copyright (c) 2015 Filipe Marques Pereira. All rights reserved.
//

#import "TaxiLoader.h"

#import "Requests.h"

@import GoogleMaps;

@implementation TaxiLoader

+ (void)loadTaxisWithCoordinate:(CLLocationCoordinate2D)coordinate
                        success:(void (^)(NSArray *taxis))success
                        failure:(void (^)(NSString *message))failure {
  [Requests getTaxisForCoordinate:coordinate
                        withBlock:^(NSArray *taxis, NSString *message) {
                          if (taxis) {
                            success(taxis);
                          } else if (message) {
                            failure(message);
                          }
                        }];
}

+ (CLLocationCoordinate2D)findClosestTaxi:(NSArray *)taxis
                                   toUser:
                                       (CLLocationCoordinate2D)userCoordinate {
  CLLocation *userLocation =
      [[CLLocation alloc] initWithLatitude:userCoordinate.latitude
                                 longitude:userCoordinate.longitude];
  CLLocationCoordinate2D nearestTaxi;
  CLLocationDistance nearestDistanceUntilNow = CLLocationDistanceMax;
  for (GMSMarker *marker in taxis) {
    // setup
    CLLocation *currentTaxiLocation =
        [[CLLocation alloc] initWithLatitude:marker.position.latitude
                                   longitude:marker.position.longitude];
    CLLocationDistance currentDistance =
        [userLocation distanceFromLocation:currentTaxiLocation];

    if (currentDistance < nearestDistanceUntilNow) {
      nearestDistanceUntilNow = currentDistance;
      nearestTaxi = marker.position;
    }
  }

  return nearestTaxi;
}

@end
